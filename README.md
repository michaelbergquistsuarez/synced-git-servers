# Custom GIT server that syncs with GitHub

# Benefits
## Backup system
It's a great backup system for public repos.

## Free semi-private repos on GitHub
Free private GitHub repos! Well, not really. But close enough. The private repos just contain the README, which in it's tur contain a reference to a private server.

# Technical
A Raspberry Pi should work well.
It should also act as a simple web server, allowing for the public to see the projects. We can start off as a simple markdown presenter in node.js.

## Security
Communication should be made through SSH.
We only allow SSH access to this server. This should be viewed as the private server. GitHub is the public server.

# Install
A remote repository is generally a bare repository — a Git repository that has no working directory.

## On the server
1. Create the directory to store repositories in `mkdir -p git/repositories` `cd git/repositories`
1. Create a new repository `mkdir project.git` `cd project.git` `git init --bare --shared=group`

### Hooks
By using server side hooks, we can syncronize repositories with another server, like GitHub.

Hooks are located in each `.git/hooks` directory. They consist of shell scripts or executable files with special names. There are three server-side hooks; `pre-recieve`, `update` and `post-recieve`.

`post-recieve` seem to be the foundation of a git syncing server. It takes a list of references from stdin. 

## On the client
1. Clone from server `git clone ssh://theserver.com/git/repositories/project.git`
1. Do stuff
1. Add the stuff `git add .`
1. Commit the stuff `git commit -m "first commit"`
1. Push `git push origin master`

# To do
- Set up git server
- How to sync?
- Need to fins a backup solution for private repos.
- Two factor auth?

# Resources
1. https://git-scm.com/book/en/v1/Git-on-the-Server
1. http://stackoverflow.com/questions/5507489/git-server-like-github
1. https://git-scm.com/book/uz/v2/Customizing-Git-Git-Hooks
1. https://github.com/pdxcat/puppet-sync - Looks interesting?
